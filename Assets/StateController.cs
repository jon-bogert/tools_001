using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : MonoBehaviour
{
    [SerializeField] int startAnimation = 0;

    void Start()
    {
        Animator animator = FindObjectOfType<Animator>();
        if (!animator)
            Debug.LogError("Issue finding animator");

        switch (startAnimation)
        {
            case 0:
                animator.Play("Idle");
                break;
            case 1:
                animator.Play("Run");
                break;
            case 2:
                animator.Play("Attack");
                break;
            case 3:
                animator.Play("Hurt");
                break;
            case 4:
                animator.Play("Death");
                break;
            case 5:
                animator.Play("jump");
                break;
        }
    }
}
